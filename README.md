# Order Service

Order service for SWA project.

## Usage

Run service from published image:

```
docker compose up
```

To see Swagger UI you can visit:
```
http://localhost:PORT/swagger-ui/index.html
```

## Logging

Logs are printed to the standard output.

## API description

### Get all items

```HTTP
GET /api/orders
```

### Add new item

- If new item does not contain following neccesarry keys: `["idCustomer", "userName","orderItems"]` the return code will be **400**. 
- In case everything is OK the return code will be **200**.

```HTTP
POST /api/orders HTTP/1.1
Content-Type: application/json
{
  "idOrder": 0,
  "idCustomer": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "userName": "string",
  "orderItems": [
    {
      "id": 0,
      "productName": "string",
      "quantity": 0
    }
  ]
}
```

### Delete item
- If some server error occurs return code will be  **404**. 
- If item with ID does not exist the return code will be **204**. 
- In case everything is OK the return code will be **200**.

```HTTP
DELETE /api/orders/{{id}} HTTP/1.1
Content-Type: application/json
```

### Update item
- If some server error occurs return code will be  **404**.
- If item with ID does not exist the return code will be **204**.
- In case everything is OK the return code will be **200**.

```HTTP
PUT /api/orders/{{id}} HTTP/1.1
Content-Type: application/json
{
  "idOrder": 0,
  "idCustomer": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "userName": "string",
  "orderItems": [
    {
      "id": 0,
      "productName": "string",
      "quantity": 0
    }
  ]
}
```
