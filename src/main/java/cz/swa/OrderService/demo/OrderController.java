package cz.swa.OrderService.demo;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:8090")
@RestController
@RequestMapping("/api")
public class OrderController {
    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ModelMapper modelMapper;
    @GetMapping("/orders")
    public ResponseEntity<List<OrderDTO>> getAllOrderItem() {
        OrderClassApplication.log.info("Get new orders called");
        try {
            List<Order> orders = (List<Order>) orderRepository.findAll();
            if (orders.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            List<OrderDTO> orderDTOs = orders.stream()
                    .map(order -> modelMapper.map(order, OrderDTO.class))
                    .collect(Collectors.toList());
            log.info("Orders returned");
            return new ResponseEntity<>(orderDTOs, HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/orders")
    public ResponseEntity<OrderDTO> createOrderItem(@RequestBody OrderDTO orderDTO) {
        OrderClassApplication.log.info("Post orders called");
        try {
            Order order = modelMapper.map(orderDTO, Order.class);
            for (OrderItem item : order.getOrderItems()) {
                item.setOrder(order);
            }
            // Save the Order entity to the database
            orderRepository.save(order);

            OrderDTO orderResponse = modelMapper.map(order,OrderDTO.class);
            return new ResponseEntity<>( orderResponse, HttpStatus.CREATED);
        } catch (Exception e) {
            log.info(e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<OrderDTO> replaceOrderItem(@RequestBody OrderDTO orderDTO, @PathVariable Long id) {
        OrderClassApplication.log.info("Put orders called");
        try {
            Optional<Order> orderData = orderRepository.findById(id);
            if (orderData.isPresent()) {
                Order order = orderData.get();
                modelMapper.map(orderDTO, order);
                orderRepository.save(order);
                OrderDTO updatedOrderDTO = modelMapper.map(order, OrderDTO.class);
                log.info("Orders changed");
                return new ResponseEntity<>(updatedOrderDTO, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.info(e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<OrderDTO> deleteOrder(@PathVariable Long id) {
        OrderClassApplication.log.info("Delete orders called");
        try {
            Optional<Order> orderData = orderRepository.findById(id);
            if (orderData.isPresent()) {
                Order order = orderData.get();
                orderRepository.delete(order);
                log.info("Orders deleted");
                return new ResponseEntity<>(null, HttpStatus.OK);
            } else {
                log.info("Orders doesn't exists");
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.info(e.toString());
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
