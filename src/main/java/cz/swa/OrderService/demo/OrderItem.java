package cz.swa.OrderService.demo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "order_items")
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    @Setter
    private String productName;

    @Getter
    @Setter
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "order_id") // This establishes a many-to-one relationship with the Order entity
    //@JsonIgnoreProperties("orderItems") // Ignore this property when serializing to JSON to prevent infinite recursion
    @Getter
    @Setter
    @JsonIgnore
    private Order order;

    public OrderItem() {
    }

    public OrderItem(String productName, int quantity,Order order) {
        this.productName = productName;
        this.quantity = quantity;
        this.order = order;
    }
}

