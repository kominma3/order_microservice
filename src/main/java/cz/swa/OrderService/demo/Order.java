package cz.swa.OrderService.demo;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;
import java.util.*;

@Entity
@Table(name = "orderitems")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long idOrder;
    @Getter
    @Setter
    private UUID idCustomer;
    @Getter
    @Setter
    private String userName;
    @Getter
    @Setter
    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL) // Define a collection for order items
    private Set<OrderItem> orderItems;
    public Order() {
    }

    public Order(UUID customerID,String userName)
    {
        this.idCustomer = customerID;
        this.userName = userName;
        this.orderItems = new HashSet<>();
    }
}
