package cz.swa.OrderService.demo;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


//TODO Testing

@CrossOrigin(origins = "http://localhost:8090")
@RestController
@RequestMapping("/api")
public class OrderItemsController {
    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private ModelMapper modelMapper;
    @GetMapping("/ordersItems")
    public ResponseEntity<List<OrderItemDTO>> getAllOrderItem() {
        OrderClassApplication.log.info("Get new ordersItems called");
        try {
            List<OrderItem> orderItems = (List<OrderItem>) orderItemRepository.findAll();
            if (orderItems.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            List<OrderItemDTO> orderItemsDTOs = orderItems.stream()
                    .map(orderItem -> modelMapper.map(orderItem, OrderItemDTO.class))
                    .collect(Collectors.toList());
            log.info("Orders returned");
            return new ResponseEntity<>(orderItemsDTOs, HttpStatus.OK);
        } catch (Exception e) {
            log.info(e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/ordersItems")
    public ResponseEntity<OrderItemDTO> createOrderItem(@RequestBody OrderItemDTO orderItemDTO) {
        try {
            // Map the DTO to an OrderItem entity
            OrderItem orderItem = modelMapper.map(orderItemDTO, OrderItem.class);

            // Save the new order item in the repository
            OrderItem savedOrderItem = orderItemRepository.save(orderItem);

            // Map the saved order item back to DTO and return it in the response
            OrderItemDTO savedOrderItemDTO = modelMapper.map(savedOrderItem, OrderItemDTO.class);

            return new ResponseEntity<>(savedOrderItemDTO, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error(e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/ordersItems/{id}")
    public ResponseEntity<OrderItemDTO> updateOrderItem(@PathVariable Long id, @RequestBody OrderItemDTO updatedOrderItemDTO) {
        try {
            // Check if the order item with the specified ID exists
            Optional<OrderItem> existingOrderItem = orderItemRepository.findById(id);

            if (existingOrderItem.isPresent()) {
                // Map the updated DTO to the existing entity
                OrderItem existingOrderItemEntity = existingOrderItem.get();
                modelMapper.map(updatedOrderItemDTO, existingOrderItemEntity);

                // Save the updated order item in the repository
                orderItemRepository.save(existingOrderItemEntity);

                // Map the updated order item back to DTO and return it in the response
                OrderItemDTO newOrderItemDTO = modelMapper.map(existingOrderItem, OrderItemDTO.class);

                return new ResponseEntity<>(newOrderItemDTO, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error(e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/ordersItems/{id}")
    public ResponseEntity<OrderItemDTO> deleteOrderItem(@PathVariable Long id) {
        try {
            // Check if the order item with the specified ID exists
            Optional<OrderItem> orderItem = orderItemRepository.findById(id);

            if (orderItem.isPresent()) {
                // Delete the order item from the repository
                orderItemRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
