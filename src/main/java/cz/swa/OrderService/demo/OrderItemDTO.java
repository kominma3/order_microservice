package cz.swa.OrderService.demo;

import lombok.Data;

import java.util.Set;
import java.util.UUID;
@Data
public class OrderItemDTO {
    private Long id;
    private String productName;
    private int quantity;
    //private Order order;
}
