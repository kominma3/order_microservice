package cz.swa.OrderService.demo;


import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class OrderDTO {
    private Long idOrder;
    private UUID idCustomer;
    private String userName;
    private Set<OrderItem> orderItems;

}
