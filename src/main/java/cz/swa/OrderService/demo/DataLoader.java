package cz.swa.OrderService.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;


@Configuration
public class DataLoader {
    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);

    @Bean
    CommandLineRunner initDatabase(OrderRepository orderRepository,OrderItemRepository orderItemRepository) {
        Order or1 = new Order(UUID.randomUUID(), "User1");
        return args -> {
            log.info("Preloading " +orderRepository.save(or1));
            log.info("Preloading " +orderItemRepository.save(new OrderItem("Motherboards",3,or1)));
            log.info("Preloading " +orderItemRepository.save(new OrderItem("GPU",2,or1)));
        };
    }

}
