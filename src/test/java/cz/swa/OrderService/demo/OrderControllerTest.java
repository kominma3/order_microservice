package cz.swa.OrderService.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.weaver.ast.Or;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
class OrderControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private OrderRepository orderRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldCreateTutorial() throws Exception {
        Order order = new Order(UUID.randomUUID(), "kominma3");
        mvc.perform(post("/api/orders").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(order)))
                .andExpect(status().isCreated()).andDo(print());
    }

    @Test
    void shouldReturnOrders() throws Exception {
        Order order = new Order(UUID.randomUUID(), "kominma3");
        List<Order> orderList = new ArrayList<>();
        orderList.add(order);
        when(orderRepository.findAll()).thenReturn(orderList);
        orderRepository.save(order);
        mvc.perform(get("/api/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(orderList.size()))
                .andDo(print());

    }

    @Test
    void shouldReturnNoContent() throws Exception {
        mvc.perform(get("/api/orders"))
                .andExpect(status().isNoContent()).andDo(print());

    }

    @Test
    void shouldUpdateTutorial() throws Exception {
        long expectedId = 12345L;
        Order order = new Order(UUID.randomUUID(), "AlanBF");
        Order updatedorder = new Order(UUID.randomUUID(), "ItisMe,Mario");
        ReflectionTestUtils.setField(order, "idOrder", expectedId);
        when(orderRepository.findById(order.getIdOrder())).thenReturn(Optional.of(order));
        ReflectionTestUtils.setField(updatedorder, "idOrder", expectedId);

        when(orderRepository.save(any(Order.class))).thenReturn(updatedorder);


        mvc.perform(put("/api/orders/{id}", order.getIdOrder()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedorder)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idOrder").value(updatedorder.getIdOrder()))
                .andExpect(jsonPath("$.idCustomer", equalToIgnoringCase(updatedorder.getIdCustomer().toString())))
                .andExpect(jsonPath("$.userName").value(updatedorder.getUserName()))
                .andDo(print());
    }

    @Test
    void shouldDeleteOrder() throws Exception {
        long id = 1L;
        Order order = new Order(UUID.randomUUID(), "AlanBF");
        ReflectionTestUtils.setField(order, "idOrder", id);
        doNothing().when(orderRepository).delete(order);
        when(orderRepository.findById(order.getIdOrder())).thenReturn(Optional.of(order));
        mvc.perform(delete("/api/orders/{id}", id))
                .andExpect(status().isOk())
                .andDo(print());
    }
}