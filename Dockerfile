#FROM openjdk:17
FROM maven:3.8.3-openjdk-17 AS build
EXPOSE 8090
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package -U
#ADD target/order-item-docker.jar order-item-docker.jar
ENTRYPOINT ["java","-jar","/home/app/target/order-item-docker.jar"]